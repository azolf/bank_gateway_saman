# frozen_string_literal: true

Saman.configure do |config|
  # You can read config from a file
  # config_hash = Rails.application.config_for(:saman)
  # config.username = config_hash[:username]
  # config.password = config_hash[:password]

  # config.proxy = config_hash[:proxy]
  # e.g. config.proxy = 'http://localhost:8080/'

  # config.terminal_id_default = config_hash[:terminal_id_default]

  # a terminal id which support multi account
  # config.terminal_id_with_wage = config_hash[:terminal_id_with_wage]

  # a termianl id which is needed for you wage account
  # config.terminal_id_wage_only = config_hash[:terminal_id_wage_only]

  # config.retry_count = config_hash[:retry_count]
  # config.authorize_wsdl = config_hash[:authorize_wsdl]
  # config.authorize_address = config_hash[:authorize_address]
  # config.verify_wsdl = config_hash[:verify_wsdl]

  # or you can set all the configs from environment variables
  # like ENV['saman_username']
end
