# frozen_string_literal: true

# Saman module for gateway communications
module Saman
  # extend Client class to handle verify method
  class Client
    def verify(params)
      return false unless params[:State] == 'OK'
      verify_the_payment(params)
    end

    private

    def verify_the_payment(params)
      savon_client = create_savon_client_for_verify
      request_parameters = {
        String_1: params[:RefNum],
        String_2: params[:TerminalId]
      }
      response = send_verify_request(savon_client, request_parameters)
      raise response[:error] if response.to_hash.include? :error
      last_response = response.body[:verify_transaction_response][:result].to_i
      last_response.positive?
    end

    def create_savon_client_for_verify
      config = Saman.configuration
      options = {
        wsdl: config.verify_wsdl,
        convert_request_keys_to: :none
      }
      options['proxy'] = config.proxy unless config.proxy.blank?

      Savon.client(options)
    end

    def send_verify_request(client, parameters, retry_to = 3)
      client.call(:verify_transaction, message: parameters)
    rescue StandardError => exception
      retry if (retry_to -= 1).positive?
      return { error: exception.message }
    end
  end
end
