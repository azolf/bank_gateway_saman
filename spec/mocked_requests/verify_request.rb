def mock_verify_request_successful
  verify_response = File.read(File.join(File.dirname(__FILE__), 'cached_xml', 'successful_verify.xml'))
  savon.expects(:verify_transaction).with(message: {
    String_1: 'gateway_reference',
    String_2: 112233
  }).returns(verify_response)
end
