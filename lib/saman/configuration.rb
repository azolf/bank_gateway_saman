# frozen_string_literal: true

# Saman module for gateway communications
module Saman
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  # configure class
  class Configuration
    attr_accessor :username
    attr_accessor :password
    attr_accessor :proxy
    attr_accessor :terminal_id_default
    attr_accessor :terminal_id_with_wage
    attr_accessor :terminal_id_wage_only
    attr_writer :retry_count
    attr_writer :authorize_wsdl
    attr_writer :authorize_address
    attr_writer :verify_wsdl

    def authorize_address
      @authorize_address || 'https://sep.shaparak.ir/MobilePG/MobilePayment'
    end

    def verify_wsdl
      @verify_wsdl || 'https://verify.sep.ir/payments/referencepayment.asmx?wsdl'
    end

    def authorize_wsdl
      @authorize_wsdl || 'https://sep.shaparak.ir/payments/initpayment.asmx?wsdl'
    end

    def retry_count
      @retry_count || 3
    end
  end
end
