# frozen_string_literal: true

require 'rails/generators'
module Saman
  module Generators
    # Install generator class for saman gem using in rails
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('../templates', __FILE__)
      desc 'Creates Saman initializer for your application'

      def copy_initializer
        template 'saman_initializer.rb', 'config/initializers/saman.rb'
      end
    end
  end
end
