def mock_verify_wsdl_request
  wsdl_path = File.join(File.dirname(__FILE__), 'cached_xml', 'verify.wsdl')
  wsdl = File.read(wsdl_path)
  stub_request(:get, Saman.configuration.verify_wsdl).
           to_return(status: 200, body: wsdl, headers: {})
end

def mock_verify_wsdl_request_timeout
  stub_request(:get, Saman.configuration.verify_wsdl).to_timeout
end