# Saman

[Coverage Report](http://amirhosein.zlf.gitlab.io/bank_gateway_saman/)

This gem will help you to handle all required requests for using Saman gateway using ruby

## Install
### Using in ruby
- Add `saman` gem to your Gemfile

`gem 'saman'`

- Run bundle install

`bundle install`

### Using in Rails
- Add `saman` gem to your Gemfile

`gem 'saman'`

- Run bundle install

`bundle install`

- Create the initializer for it

`bundle exec rails g saman:install`

### How to use it?

#### Configurations

For `rails` applications it will generate an `initializer` file called `saman.rb` which you could set all the required configurations. 

If you are using this gem in a `ruby`
script, you should set the configurations like below before calling any methods.

```
Saman.configure do |config|
  # config.username = config_hash[:username]
  # config.password = config_hash[:password]

  # config.proxy = config_hash[:proxy]
  # e.g. config.proxy = 'http://localhost:8080/'

  # config.terminal_id_default = config_hash[:terminal_id_default]

  # a terminal id which support multi account
  # config.terminal_id_with_wage = config_hash[:terminal_id_with_wage]

  # a termianl id which is needed for you wage account
  # config.terminal_id_wage_only = config_hash[:terminal_id_wage_only]

  # config.retry_count = config_hash[:retry_count]
  # config.authorize_wsdl = config_hash[:authorize_wsdl]
  # config.authorize_address = config_hash[:authorize_address]
  # config.verify_wsdl = config_hash[:verify_wsdl]
end

```

#### Is gateway ready?
You can check if the Saman gateway is ready or not, this method always return a `boolean` value

`Saman.up?`

#### How get a token from gateway?
The `authorize` method get a token for you from bank and it returns a response like below
`Saman.authorize(params)`
```
{
  method: 'POST',
  fields: {
    Token: 'generated_token'
  },
  url: 'http://sample.com/'
}
```
This means you should submit a from by `POST` request to `http://sample.com/` with a field named `Token` and value of `generated_token`

The required params for generating a token are like below
```
{
  "amount": 3000,
  "order_id": 128,
  "customer": {
    "mobile_number": "09128987989"
  },
  "split_amount": {
    "amount": 1500,
    "wage": 1500
  },
  "redirect_url": "http://localhost"
}
```
NOTE: if you do not want to use `split_amount` feature, just pass the `amount` and ignore that.


#### How verify a payment?

For verifying a payment, you just need to pass the parameters which were sending to you from gateway like below and this method returns a boolean after calling verify.

`Saman.verify(posted_params)`

### Development
if you want to change the gem, you could easily clone it and do whatever you want

After your changes always run `specs` and `rubocop` to make sure about the code quality

- `rspec spec`
- `rubocop --format html > rubocop.html`