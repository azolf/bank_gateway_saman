require_relative '../../lib/saman'

RSpec.describe Saman do
  before(:each) do
    Saman.configure {}
    mock_authorize_wsdl_request
    mock_verify_wsdl_request
  end
  
  context "health check " do
    it 'is up and working' do
      expect(Saman.up?).to be true
    end
    
    it 'gets timeout when tries to reach authorize wsdl' do
      mock_authorize_wsdl_request_timeout
      expect(Saman.up?).to be false
    end

    it 'gets timeout when tries to reach verify wsdl' do
      mock_verify_wsdl_request_timeout
      expect(Saman.up?).to be false
    end
  end
end