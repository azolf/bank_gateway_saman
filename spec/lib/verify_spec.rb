require_relative '../../lib/saman'

RSpec.describe Saman do
  include Savon::SpecHelper
  context "verifying a payment" do
    before(:each) do
      Saman.configure {}
      savon.mock!
    end

    after(:each) do
      savon.unmock!
    end

    let(:params) do
      {
        ResNum: 456,
        RefNum: 'gateway_reference',
        TerminalId: 112233,
        State: 'OK'
      }
    end
    
    it 'verifies successfully' do
      mock_verify_wsdl_request
      mock_verify_request_successful
      result = Saman.verify(params)
      expect(result).to be true
    end
    
    it 'gets an exception' do
      mock_verify_wsdl_request_timeout
      expect{Saman.verify(params)}.to raise_error(RuntimeError)
    end
  end
end