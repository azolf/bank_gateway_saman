require_relative '../../lib/saman'

RSpec.describe Saman do
  before(:each) do
    Saman.configure do |config|
      config.username = 'test_username'
      config.password = 'test_password'
      config.proxy = 'http://testproxy.local'
      config.terminal_id_default = 'terminal_id_default'
      config.terminal_id_with_wage = 'terminal_id_with_wage'
      config.terminal_id_wage_only = 'terminal_id_wage_only'
    end
  end
  
  context 'authorize without multiple accounts' do
    let(:params) do
      {
        amount: 3000,
        order_id: 123456,
        customer: {
          id: 1,
          mobile_number: "09121234567"
        },
        redirect_url: 'http://localhost'
      }
    end
    
    it 'gets token successfully' do
      mock_authorize_request
      expected_response = {
        method: 'POST',
        fields: {
          Token: 'this is your token'
        },
        url: Saman.configuration.authorize_address
      }
      
      result = Saman.authorize(params)
      expect(result).to eq expected_response
    end
    
    it 'gets timeout for getting token' do
      mock_authorize_request_timeout
      error_message = 'Saman is not available right now,
                                            calling web service got time out'
      expect{ Saman.authorize(params) }.to \
                                        raise_error(RuntimeError, error_message)
    end
    
    it 'gets StandardError for getting token' do
      mock_authorize_request_general_exception
      error_message = 'My Error!!!'
      expect{ Saman.authorize(params) }.to \
                                        raise_error(RuntimeError, error_message)
    end
    
    it 'gets an error from webservice response' do
      mock_authorize_request_unsuccessful
      error_message = 'Code #5, Something went wrong!'
      expect{ Saman.authorize(params) }.to \
                                        raise_error(RuntimeError, error_message)
    end
    
  end
  
  context 'authorize with multiple accounts' do
    let(:params) do
      {
        amount: 3000,
        order_id: 123456,
        customer: {
          id: 1,
          mobile_number: "09121234567"
        },
        'split_amount': {
          amount: 1200,
          wage: 1800
        },
        redirect_url: 'http://localhost'
      }
    end
    
    it 'gets token successfully' do
      mock_authorize_request_for_multiple_accounts
      expected_response = {
        method: 'POST',
        fields: {
          Token: 'this is your token for multiple accounts'
        },
        url: Saman.configuration.authorize_address
      }
      
      result = Saman.authorize(params)
      expect(result).to eq expected_response
    end
  end
  
  context 'authorize with one account only' do
    let(:params_for_default_account) do
      {
        amount: 3000,
        order_id: 123456,
        customer: {
          id: 1,
          mobile_number: "09121234567"
        },
        'split_amount': {
          amount: 3000
        },
        redirect_url: 'http://localhost'
      }
    end
    
    let(:params_wage_only) do
      {
        amount: 3000,
        order_id: 123456,
        customer: {
          id: 1,
          mobile_number: "09121234567"
        },
        'split_amount': {
          wage: 3000
        },
        redirect_url: 'http://localhost'
      }
    end
    
    it 'gets token successfully for default account only' do
      mock_authorize_request
      expected_response = {
        method: 'POST',
        fields: {
          Token: 'this is your token'
        },
        url: Saman.configuration.authorize_address
      }
      
      result = Saman.authorize(params_for_default_account)
      expect(result).to eq expected_response
    end

    it 'gets token successfully for wage account only' do
      mock_authorize_request_wage_only
      expected_response = {
        method: 'POST',
        fields: {
          Token: 'this is your token'
        },
        url: Saman.configuration.authorize_address
      }
      
      result = Saman.authorize(params_wage_only)
      expect(result).to eq expected_response
    end
  end
end